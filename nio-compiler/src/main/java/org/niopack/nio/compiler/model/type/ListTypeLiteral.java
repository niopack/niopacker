package org.niopack.nio.compiler.model.type;

public final class ListTypeLiteral implements TypeLiteral {

  private ScalarTypeLiteral elementType;

  public ScalarTypeLiteral getElementType() {
    return elementType;
  }

  public void setElementType(ScalarTypeLiteral elementType) {
    this.elementType = elementType;
  }

  @Override
  public Type getType() {
    return Type.LIST;
  }

  @Override
  public String toString() {
    return "[" + getElementType() + "]";
  }
}
