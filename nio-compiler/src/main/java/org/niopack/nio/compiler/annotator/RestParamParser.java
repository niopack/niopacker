package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.ParamType;
import org.niopack.nio.core.RestParam;
import org.niopack.nio.core.RestParam_Nio;

import java.util.IllegalFormatException;
import java.util.Set;

public class RestParamParser extends AbstractAnnotationParser<RestParam> {

  public RestParamParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.FIELD_STRUCT);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.RestParam";
  }

  @Override
  protected RestParam parse(StructureValue value) throws IllegalFormatException {
    RestParam annotation = new RestParam();
    if (value.hasValue(RestParam_Nio.TYPE)) {
      annotation.setType(value.scalarValue(RestParam_Nio.TYPE).toEnum(ParamType.class));
    }
    if (value.hasValue(RestParam_Nio.NAME)) {
      annotation.setName(value.scalarValue(RestParam_Nio.NAME).toString());
    }
    return annotation;
  }
}
