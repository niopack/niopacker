package org.niopack.nio.compiler.language.java.model;

import org.niopack.nio.compiler.common.Formatter;

public final class JavaStructureField {
  private final String name;
  private final String nameConstant;
  private final String javaType;
  private final String defaultValue;
  private final String restParamType;
  private final String restParamName;

  public JavaStructureField(String name, String javaType, String defaultValue,
      String restParamType, String restParamName) {
    this.name = name;
    this.nameConstant = Formatter.toUpperUnderscore(name);
    this.javaType = javaType;
    this.defaultValue = defaultValue;
    this.restParamType = restParamType;
    this.restParamName = restParamName;
  }

  public String getName() {
    return name;
  }

  public String getNameConstant() {
    return nameConstant;
  }

  public String getJavaType() {
    return javaType;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public String getRestParamType() {
    return restParamType;
  }

  public String getRestParamName() {
    return restParamName;
  }
}
