package org.niopack.nio.compiler.language.php.model;

public final class PhpEnumerationField {
  private final String name;
  private final int value;

  public PhpEnumerationField(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public int getValue() {
    return value;
  }
}
