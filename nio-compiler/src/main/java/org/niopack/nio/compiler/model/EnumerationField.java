package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class EnumerationField extends AbstractAnnotatedIdentifier {

  private int value;

  public EnumerationField(String name, int value) {
    super(name, new ScalarTypeLiteral(Type.NONE, name));
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  @Override
  public Category getCategory() {
    return Category.FIELD_ENUM;
  }
}
