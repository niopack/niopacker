package org.niopack.nio.compiler.language.java;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

public class NameGenerator {

  private final Multiset<String> names = HashMultiset.create();

  public NameGenerator() {
    reset();
  }

  public String getName(String nameSeed) {
    int previousCount = names.add(nameSeed, 1);
    if (previousCount == 0) {
      return nameSeed;
    } else {
      String name = nameSeed + "_" + (previousCount + 1);
      names.add(name);
      return name;
    }
  }

  public void reset() {
    names.clear();
    // avoid generating variable names matching java keywords.
    names.addAll(JavaSymbolFactory.KEYWORDS);
  }
}
