package org.niopack.nio.compiler.model.type;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public final class ScalarTypeLiteral implements TypeLiteral {

  private final Type type;
  private final String typeName;

  public ScalarTypeLiteral(Type type) {
    checkNotNull(type, "type");
    checkNotNull(type.getConcreteType(), "Must specify typeName for non built-in types.");
    this.type = type;
    this.typeName = type.getConcreteType();
  }

  public ScalarTypeLiteral(Type type, String typeName) {
    checkNotNull(type, "type");
    checkArgument(type == Type.USER_DEFINED || type == Type.NONE,
        "%s is not USER_DEFINED | NONE.", type);
    this.type = type;
    this.typeName = typeName;
  }

  public Type getType() {
    return type;
  }

  public String getTypeName() {
    return typeName;
  }

  @Override
  public String toString() {
    return typeName;
  }
}
