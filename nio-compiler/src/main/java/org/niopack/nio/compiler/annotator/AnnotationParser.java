package org.niopack.nio.compiler.annotator;

import javax.annotation.Nullable;

import static org.niopack.nio.compiler.model.Symbol.DOT;

public interface AnnotationParser<A> {

  @Nullable
  A getAnnotation();

  /**
   * Gets canonical name of annotation
   */
  String getCanonicalName();

  /**
   * Gets simple name of annotation
   */
  default String getSimpleName() {
    String canonicalName = getCanonicalName();
    return canonicalName.substring(canonicalName.lastIndexOf(DOT) + 1);
  }
}
