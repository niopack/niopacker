package org.niopack.nio.compiler.language.php.model;

import org.niopack.nio.core.ParamType;

public final class PhpStructureField {
  private final String name;
  private final TypeInfo typeInfo;
  private final ParamType restParamType;
  private final String restParamName;

  public PhpStructureField(String name, TypeInfo typeInfo, ParamType restParamType,
      String restParamName) {
    this.name = name;
    this.typeInfo = typeInfo;
    this.restParamType = restParamType;
    this.restParamName = restParamName;
  }

  public String getName() {
    return name;
  }

  public TypeInfo getTypeInfo() {
    return typeInfo;
  }

  public ParamType getRestParamType() {
    return restParamType;
  }

  public String getRestParamName() {
    return restParamName;
  }
}
