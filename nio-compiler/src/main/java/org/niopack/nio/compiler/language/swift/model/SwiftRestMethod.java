package org.niopack.nio.compiler.language.swift.model;

import org.niopack.nio.core.HttpVerb;

public final class SwiftRestMethod {
  private final String name;
  private final TypeInfo requestType;
  // private final Set<ParamType> requestParamTypes;
  private final TypeInfo responseType;
  private final HttpVerb verb;
  private final String path;

  public SwiftRestMethod(String name, TypeInfo requestType, TypeInfo responseType, HttpVerb verb,
      String path) {
    this.name = name;
    this.requestType = requestType;
    this.responseType = responseType;
    this.verb = verb;
    this.path = path;
  }

  public String getName() {
    return name;
  }

  public TypeInfo getRequestType() {
    return requestType;
  }

  public TypeInfo getResponseType() {
    return responseType;
  }

  public HttpVerb getVerb() {
    return verb;
  }

  public String getPath() {
    return path;
  }
}
