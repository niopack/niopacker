package org.niopack.nio.compiler.language.java.model;

public final class JavaRestService {
  private String path;
  private String produces;

  public JavaRestService(String path, String produces) {

    this.path = path;
    this.produces = produces;
  }

  public String getPath() {
    return path;
  }

  public String getProduces() {
    return produces;
  }
}
