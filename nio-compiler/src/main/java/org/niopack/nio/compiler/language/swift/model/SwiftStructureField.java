package org.niopack.nio.compiler.language.swift.model;

import org.niopack.nio.core.ParamType;

public final class SwiftStructureField {
  private final String name;
  private final TypeInfo typeInfo;
  private final ParamType restParamType;
  private final String restParamName;

  public SwiftStructureField(String name, TypeInfo typeInfo, ParamType restParamType,
      String restParamName) {
    this.name = name;
    this.typeInfo = typeInfo;
    this.restParamType = restParamType;
    this.restParamName = restParamName;
  }

  public String getName() {
    return name;
  }

  public TypeInfo getTypeInfo() {
    return typeInfo;
  }

  public ParamType getRestParamType() {
    return restParamType;
  }

  public String getRestParamName() {
    return restParamName;
  }
}
