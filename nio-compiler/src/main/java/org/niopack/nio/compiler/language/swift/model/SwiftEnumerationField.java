package org.niopack.nio.compiler.language.swift.model;

public final class SwiftEnumerationField {
  private final String name;
  private final int value;

  public SwiftEnumerationField(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public String getNameConstant() {
    return name.toUpperCase();
  }

  public String getName() {
    return name;
  }

  public int getValue() {
    return value;
  }
}
