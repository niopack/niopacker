package org.niopack.nio.compiler.language.java;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.niopack.nio.compiler.annotator.JavaParser;
import org.niopack.nio.compiler.annotator.ProvidedParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Package;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.Java;
import org.niopack.nio.core.Provided;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Collectors;

public interface JavaSymbol {

  public static final String DOT = ".";

  Symbol getSymbol();
  SymbolTable getTable();
  <T> T getDefinition();

  default String getPackageName() {
    Package pkg = (Package) Iterables.getFirst(getSymbol().getIdentifiers(), null);
    if (pkg != null) {
      Java java = new JavaParser(pkg, getTable()).getAnnotation();
      if (java != null) {
        String packageName = java.getPackageName();
        if (packageName != null) {
          return packageName;
        }
      }
    }
    return "";
  }

  default String getCanonicalName() {
    String subName = getSymbol().getIdentifiers().stream()
        .skip(1)
        .map(id -> id.getName())
        .collect(Collectors.joining(DOT));
    String packageName = getPackageName();
    if (packageName.isEmpty()) {
      return subName;
    } else {
      return packageName + DOT + subName;
    }
  }

  default String getSimpleName() {
    return Iterables.getLast(getSymbol().getIdentifiers()).getName();
  }

  Category getCategory();

  default boolean isProvided() {
    return false;
  }

  Map<String, Object> getSourceDataModel();

  void writeSource(Configuration cfg, Writer writer) throws TemplateException, IOException;

  default void writeSource(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    String relativePath = getCanonicalName().replace(DOT, File.separator) + ".java";
    Path outputFile = outputDirectory.resolve(relativePath);
    if (Files.notExists(outputFile.getParent())) {
      Files.createDirectories(outputFile.getParent());
    }
    writeSource(cfg, Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8));
  }

  Map<String, Object> getNioDataModel(Configuration cfg);

  void writeNio(Configuration cfg, Writer writer) throws IOException, TemplateException;

  default void writeNio(Configuration cfg, Path outputDirectory)
      throws IOException, TemplateException {
    if (isProvided()) {
      return;
    }
    String relativePath = getCanonicalName().replace(DOT, File.separator) + "_Nio.java";
    Path outputFile = outputDirectory.resolve(relativePath);
    if (Files.notExists(outputFile.getParent())) {
      Files.createDirectories(outputFile.getParent());
    }
    writeNio(cfg, Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8));
  }
}
