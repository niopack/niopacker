package org.niopack.nio.compiler.language.java.model;

import org.niopack.nio.compiler.common.Formatter;

public final class JavaRestMethod {
  private final String name;
  private final String nameConstant;
  private final String requestType;
  private final String responseType;
  private final String verb;
  private final String path;

  public JavaRestMethod(String name, String requestType, String responseType, String verb,
      String path) {
    this.name = name;
    this.nameConstant = Formatter.toUpperUnderscore(name);
    this.requestType = requestType;
    this.responseType = responseType;
    this.verb = verb;
    this.path = path;
  }

  public String getName() {
    return name;
  }

  public String getNameConstant() {
    return nameConstant;
  }

  public String getRequestType() {
    return requestType;
  }

  public String getResponseType() {
    return responseType;
  }

  public String getVerb() {
    return verb;
  }

  public String getPath() {
    return path;
  }
}
