package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Php;
import org.niopack.nio.core.Php_Nio;

import java.util.IllegalFormatException;
import java.util.Set;

import static org.niopack.nio.compiler.common.Formatter.unescape;

public class PhpParser extends AbstractAnnotationParser<Php> {

  public PhpParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.PACKAGE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Php";
  }

  @Override
  protected Php parse(StructureValue value) throws IllegalFormatException {
    Php annotation = new Php();
    if (value.hasValue(Php_Nio.NAMESPACE)) {
      annotation.setNamespace(unescape(value.scalarValue(Php_Nio.NAMESPACE).toString()));
    }
    return annotation;
  }
}
