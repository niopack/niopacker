grammar Nio;

@header {
package org.niopack.nio.compiler.antlr4;
}

init
    :   annotation* pkgDecl (importDecl)* (structure | enumeration | service)* EOF
    ;

pkgDecl
    :   PACKAGE packageName ';'
    ;

packageName
    :   Identifier (DOT Identifier)*
    ;

importDecl
    :   IMPORT StringLiteral ';'
    ;

enumeration
    :   annotation* ENUM Identifier '{'
            enumerationField (',' enumerationField)*
        '}'
    ;

enumerationField
    :   annotation* Identifier '=' IntegerLiteral
    ;

service
    :   annotation* SERVICE Identifier '{'
            serviceMethod+
        '}'
    ;

serviceMethod
    :   annotation* serviceResponse Identifier '(' serviceRequest ')' ';'
    ;

serviceRequest
    :   Identifier
    |   fullyQualifiedType
    ;

serviceResponse
    :   VOID | scalarTypes
    ;

structure
    :   annotation* STRUCT Identifier '{'
            (   scalarField
            |   multiValueField
            // |   enumeration
            // |   structure
            )*
        '}'
    ;

scalarField
    : annotation* scalarTypes Identifier ( '=' value )? ';'
    ;

multiValueField
    : annotation* multiValueTypes Identifier ';'
    ;

multiValueTypes
    :   listType
    |   mapType
    ;

listType
    :   '[' scalarTypes ']'
    ;

mapType
    :   '<' scalarTypes ',' scalarTypes '>'
    ;

scalarTypes
    :   builtinTypes
    |   Identifier
    |   fullyQualifiedType
    ;

builtinTypes
    :   BOOLEAN
    |   BYTES
    |   CHAR
    |   STRING
    |   INT16
    |   INT32
    |   INT64
    |   INT8
    |   MSEC
    |   USEC
    |   DOUBLE
    |   FLOAT
    ;

fullyQualifiedType
    :   (DOT Identifier)+
    ;

// No marker annotation
annotation
    :   normalAnnotation
    |   singleElementAnnotation
    ;

normalAnnotation
    :   AT Identifier structureValue
    ;

singleElementAnnotation
    :   AT Identifier '(' value ')'
    ;

// support empty objrct () ?
structureValue
    :   '(' pair (',' pair)* ')'
    |   '(' ')'
    ;

pair
    :   Identifier '=' value
    ;

value
    :   CharacterLiteral
    |   StringLiteral
    |   IntegerLiteral
    |   FloatingPointLiteral
    |   structureValue  // recursion
    |   BooleanLiteral
    |   Identifier
    ;

// LEXER

// Keywords

PACKAGE         : 'package';
INTERFACE       : 'interface';
IMPORT          : 'import';

// Data types
BOOLEAN         : 'bool';
BYTES           : 'bytes';
CHAR            : 'char';
DOUBLE          : 'double';
ENUM            : 'enum';
FLOAT           : 'float';
INT16           : 'int16';
INT32           : 'int32';
INT64           : 'int64';
INT8            : 'int8';
UINT16          : 'uint16';
UINT32          : 'uint32';
UINT64          : 'uint64';
UINT8           : 'uint8';
MSEC            : 'msec';
SERVICE         : 'service';
STRING          : 'string';
STRUCT          : 'struct';
UNION           : 'union';
USEC            : 'usec';
VOID            : 'void';

// Integer Literals

IntegerLiteral
    :   '-'? INT EXP             // 1e10 -3e4
    |   '-'? INT                 // -3, 45
    ;

// Floating-Point Literals (also copies integer literal)

FloatingPointLiteral
    :   '-'? INT '.' [0-9]+ EXP? // 1.35, 1.35E-9, 0.3, -4.5
    |   '-'? INT EXP             // 1e10 -3e4
    |   '-'? INT                 // -3, 45
    ;

fragment
INT
    :   '0' | [1-9] [0-9]* // no leading zeros
    ;

fragment
EXP
    :   [Ee] [+\-]? INT // \- since - means "range" inside [...]
    ;

// Boolean Literals

BooleanLiteral
    :   'true'
    |   'false'
    ;

// Character Literals

CharacterLiteral
    :   '\'' SingleCharacter '\''
    |   '\'' ESC '\''
    ;

fragment
SingleCharacter
    :   ~['\\]
    ;

// String Literals

StringLiteral
    :   '"' (ESC | ~["\\])* '"'
    ;
fragment
ESC
    :   '\\' (["\\/bfnrt] | UNICODE)
    ;
fragment
UNICODE
    : 'u' HEX HEX HEX HEX
    ;
fragment
HEX
    : [0-9a-fA-F]
    ;

// Separators
LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
DOT    : '.';
GT     : '>';
LT     : '<';

// Identifiers (must appear after all keywords in the grammar)

Identifier
    :   NonDigit
        (   NonDigit
        |   Digit
        )*
    ;

fragment
NonDigit
    :   [a-zA-Z_]
    ;

fragment
Digit
    :   [0-9]
    ;

AT  : '@';

//
// Whitespace and comments
//

WS
    :   [ \t\r\n]+
        -> skip
    ;

BLOCK_COMMENT
    :   '/*' .*? '*/'
        -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]*
        -> skip
    ;
