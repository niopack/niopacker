<?php<#if namespace?has_content> namespace ${namespace};</#if>

final class ${typeName}
{
    <#list fields as field>
    const ${field.name} = '${field.name}';
    </#list>
    private static $values = [
        <#list fields as field>
        '${field.name}' => ${field.value}<#if field_has_next>,</#if>
        </#list>
    ];

    private function __construct()
    {
    }

    public static function values()
    {
        return self::$values;
    }

    public static function value($name)
    {
        return self::values()[$name];
    }

    public static function name($value)
    {
        $name = array_search($value, self::values(), true);
        assert($name !== false);
        return $name;
    }
}
