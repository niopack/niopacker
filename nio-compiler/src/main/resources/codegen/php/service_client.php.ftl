<?php<#if namespace?has_content> namespace ${namespace};</#if>

<#list imports as import>
use ${import?substring(1)};
<#if !import_has_next>

</#if>
</#list>
final class ${typeName}Client
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    <#list methods as method>
    public function ${method.name}(${method.requestType} $request, array $options = [])
    {
        try {
            <#list method.requestFields as field>
            <#if field.restParamType == "PATH">
            $${field.restParamName} = $request->${field.name}<#if field.typeInfo.canonicalName != ''>->jsonSerialize()</#if>;
            </#if>
            </#list>
            $url = "${method.path}";
            <#list method.requestFields as field>
            <#if field.restParamType == "FORM">
            if (isset($request->${field.name})) {
                $options['body']['${field.restParamName}'] = $request->${field.name}<#if field.typeInfo.canonicalName != ''>->jsonSerialize()</#if>;
            }
            </#if>
            </#list>
            // Always use synchronous requests
            unset($options['future']);
            $request = $this->client->createRequest('${method.verb?upper_case}', $url, $options);
            <#list method.requestFields as field>
            <#if field.restParamType == "QUERY">
            $query = $request->getQuery();
            $query->setAggregator($query::duplicateAggregator());
            <#break>
            </#if>
            </#list>
            <#list method.requestFields as field>
            <#if field.restParamType == "QUERY" && field.typeInfo.array>
            if (isset($request->${field.name})) {
                foreach ($request->${field.name} as $value) {
                    $options['query']['${field.restParamName}'] = $value<#if field.typeInfo.canonicalName != ''>->jsonSerialize()</#if>;
                }
            }
            <#elseif field.restParamType == "QUERY" && !field.typeInfo.array>
            if (isset($request->${field.name})) {
                $options['query']['${field.restParamName}'] = $request->${field.name}<#if field.typeInfo.canonicalName != ''>->jsonSerialize()</#if>;
            }
            </#if>
            </#list>
            return Response::of($this->client->send($request), '${method.responseType}');
        } catch (\Exception $e) {
            return Response::fromException($e, '${method.responseType}');
        }
    }

    </#list>
}
