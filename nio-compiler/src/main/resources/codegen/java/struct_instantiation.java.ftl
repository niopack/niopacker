    ${name} ${variableName} = new ${name}();
    <#list assignments?keys as field>
    ${variableName}.set${field}(${assignments[field]});
    </#list>