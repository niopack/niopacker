<#list modules as module>
import ${module}
</#list>

public class ${typeName} {
    <#list fields as field>
    <#if field.typeInfo.fieldType = "LIST">
    var ${field.name}: [${field.typeInfo.keyTypeName}] = []
    <#elseif field.typeInfo.fieldType = "MAP">
    var ${field.name}: [${field.typeInfo.keyTypeName}: ${field.typeInfo.valueTypeName}] = [:]
    <#else>
    var ${field.name}: ${field.typeInfo.fieldTypeName}?
    </#if>
    </#list>

    init() {
    }

    init(json: [String: AnyObject]) {
        <#list fields as field>
        <#if field.typeInfo.fieldType = "LIST">
        if let __${field.name}__ = json["${field.name}"] as? [AnyObject] {
            for __item__ in __${field.name}__ {
                <#if field.typeInfo.keyType = "ENUM">
                ${field.name}.append(${field.typeInfo.keyTypeName}.fromName(__item__)!)
                <#elseif field.typeInfo.keyType = "STRUCT">
                ${field.name}.append(${field.typeInfo.keyTypeName}(json: __item__ as [String: AnyObject]))
                <#else>
                ${field.name}.append(__item__ as ${field.typeInfo.keyTypeName})
                </#if>
            }
        }
        <#elseif field.typeInfo.fieldType = "MAP">
        if let __${field.name}__ = json["${field.name}"] as? [String: AnyObject] {
            for (__key__, __value__) in __${field.name}__ {
                <#assign valueFragment>
                <#if field.typeInfo.valueType = "ENUM">
                <#t>${field.typeInfo.valueTypeName}.fromName(__value__)!<#t>
                <#elseif field.typeInfo.valueType = "STRUCT">
                <#t>${field.typeInfo.valueTypeName}(json: __value__ as [String: AnyObject])<#t>
                <#else>
                <#t>(__value__ as ${field.typeInfo.valueTypeName})<#t>
                </#if>
                </#assign>
                <#if field.typeInfo.keyType = "ENUM">
                ${field.name}[${field.typeInfo.keyTypeName}.fromName(__key__)!] = ${valueFragment}
                <#elseif field.typeInfo.keyType = "STRING">
                ${field.name}[__key__ as String] = ${valueFragment}
                <#else>
                ${field.name}[(__key__ as String).toInt()!] = ${valueFragment}
                </#if>
            }
        }
        <#else>
        <#if field.typeInfo.fieldType = "ENUM">
        if let __${field.name}__ = json["${field.name}"] as? String {
            ${field.name} = ${field.typeInfo.fieldTypeName}.fromName(__${field.name}__)
        }
        <#elseif field.typeInfo.fieldType = "STRUCT">
        if let __${field.name}__ = json["${field.name}"] as? [String: AnyObject] {
            ${field.name} = ${field.typeInfo.fieldTypeName}(json: __${field.name}__)
        }
        <#else>
        ${field.name} = json["${field.name}"] as? ${field.typeInfo.fieldTypeName}
        </#if>
        </#if>
        </#list>
    }

    func toJson() -> [String: AnyObject] {
        <#if fields?has_content>
        var json: [String: AnyObject] = [:]
        <#list fields as field>
        <#if field.typeInfo.fieldType = "LIST">
        if !${field.name}.isEmpty {
            var __${field.name}__: [AnyObject] = []
            for __item__ in ${field.name} {
                <#if field.typeInfo.keyType = "ENUM">
                __${field.name}__.append(__item__.name)
                <#elseif field.typeInfo.keyType = "STRUCT">
                __${field.name}__.append(__item__.toJson())
                <#else>
                __${field.name}__.append(__item__)
                </#if>
            }
            json["${field.name}"] = __${field.name}__
        }
        <#elseif field.typeInfo.fieldType = "MAP">
        if !${field.name}.isEmpty {
            var __${field.name}__: [String: AnyObject] = [:]
            for (__key__, __value__) in ${field.name} {
                <#assign keyFragment>
                <#if field.typeInfo.keyType = "ENUM">
                <#t>__key__.name<#t>
                <#elseif field.typeInfo.keyType = "STRING">
                <#t>__key__<#t>
                <#else>
                <#t>String(__key__)<#t>
                </#if>
                </#assign>
                <#if field.typeInfo.valueType = "ENUM">
                __${field.name}__[${keyFragment}] = __value__.name
                <#elseif field.typeInfo.valueType = "STRUCT">
                __${field.name}__[${keyFragment}] = __value__.toJson()
                <#else>
                __${field.name}__[${keyFragment}] = __value__
                </#if>
            }
            json["${field.name}"] = __${field.name}__
        }
        <#else>
        <#if field.typeInfo.fieldType = "ENUM">
        if let __${field.name}__ = ${field.name} {
            json["${field.name}"] = __${field.name}__.name
        }
        <#elseif field.typeInfo.fieldType = "STRUCT">
        if let __${field.name}__ = ${field.name} {
            json["${field.name}"] = __${field.name}__.toJson()
        }
        <#else>
        if let __${field.name}__ = ${field.name} {
            json["${field.name}"] = __${field.name}__;
        }
        </#if>
        </#if>
        </#list>
        return json;
        <#else>
        return [String: AnyObject]();
        </#if>
    }
}
