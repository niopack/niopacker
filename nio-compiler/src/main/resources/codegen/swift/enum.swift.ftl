public enum ${typeName}: Int, Equatable, Printable {
    <#list fields as field>
    case ${field.name} = ${field.value}
    </#list>

    static func fromName(name: AnyObject) -> ${typeName}? {
        if let __name__ = name as? String {
            switch __name__ {
            <#list fields as field>
            case "${field.name}":
                return DayOfWeek.${field.name}
            </#list>
            default:
                return nil
            }
        }
        return nil
    }

    public var name: String {
        return self.description
    }

    public var description: String {
        switch self {
        <#list fields as field>
        case .${field.name}:
            return "${field.name}"
        </#list>
        }
    }
}

public func ==(lhs: ${typeName}, rhs: ${typeName}) -> Bool {
    return lhs.rawValue == rhs.rawValue
}
