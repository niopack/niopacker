#!/bin/bash
set -x

gradle clean install installapp
sudo ln -sf $PWD/nio-compiler/build/install/nio-compiler/bin/nio-compiler /usr/local/bin/niopacker
