<?php namespace Niopack;

use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Message\ResponseInterface;

final class Response {

    private $rawResponse;
    private $apiResponseClass;
    private $exceptionMessage;

    private function __construct(){
    }

    /**
     * @param Exception $e
     * @return Response
     */
    public static function fromException(Exception $e, $apiResponseClass) {
        $response = new Response();
        $response->apiResponseClass = $apiResponseClass;
        if ($e instanceof RequestException && $e->hasResponse()) {
            $response->rawResponse = $e->getResponse();
        } else {
            $response->exceptionMessage = $e->getMessage();
        }
        return $response;
    }

    /**
     * @param ResponseInterface $rawResponse
     * @param $apiResponseClass
     * @return Response
     */
    public static function of(ResponseInterface $rawResponse, $apiResponseClass) {
        $response = new Response();
        $response->rawResponse = $rawResponse;
        $response->apiResponseClass = $apiResponseClass;
        return $response;
    }

    /**
     * @return bool
     */
    public function hasRawResponse()
    {
        return isset($this->rawResponse);
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return isset($this->rawResponse)
            && $this->rawResponse->getStatusCode() >= 200
            && $this->rawResponse->getStatusCode() < 300;
    }

    /**
     * @return bool
     */
    public function isProtocolError()
    {
        return isset($this->rawResponse)
        && $this->rawResponse->getStatusCode() < 200
        && $this->rawResponse->getStatusCode() >= 300;
    }

    /**
     * @return mixed
     */
    public function getApiResponse()
    {
        if ($this->apiResponseClass === '') {
            return $this->rawResponse->getBody();
        }
        return new $this->apiResponseClass($this->rawResponse->json());
    }

    /**
     * @return mixed
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @return mixed
     */
    public function getExceptionMessage()
    {
        return $this->exceptionMessage;
    }
}