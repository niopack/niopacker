package org.niopack.nio.core;

public enum HttpVerb {

  GET(1),
  POST(2),
  PUT(3),
  DELETE(4);

  private final int value;

  HttpVerb(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
