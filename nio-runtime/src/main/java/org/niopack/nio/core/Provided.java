package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Provided {

  private boolean provided;

  public boolean isProvided() {
    return provided;
  }

  public void setProvided(boolean provided) {
    this.provided = provided;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(provided);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Provided)) {
      return false;
    }
    Provided that = (Provided) obj;
    return Objects.equal(this.provided, that.provided);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("provided", provided)
        .toString();
  }
}
