package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Php {
  private String namespace;

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(namespace);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Php)) {
      return false;
    }
    Php that = (Php) obj;
    return Objects.equal(this.namespace, that.namespace);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("namespace", namespace)
        .toString();
  }
}
