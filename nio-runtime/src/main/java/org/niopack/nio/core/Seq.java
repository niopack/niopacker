package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Seq {
  private int pos;

  public int getPos() {
    return pos;
  }

  public void setPos(int pos) {
    this.pos = pos;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(pos);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Seq)) {
      return false;
    }
    Seq that = (Seq) obj;
    return Objects.equal(this.pos, that.pos);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("pos", pos)
        .toString();
  }
}
