package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Description {

  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(description);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Description)) {
      return false;
    }
    Description that = (Description) obj;
    return Objects.equal(this.description, that.description);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("description", description)
        .toString();
  }
}
