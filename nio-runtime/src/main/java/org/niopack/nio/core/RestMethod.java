package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class RestMethod {
  private HttpVerb verb;
  private String path;

  public HttpVerb getVerb() {
    return verb;
  }

  public void setVerb(HttpVerb verb) {
    this.verb = verb;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(verb, path);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RestMethod)) {
      return false;
    }
    RestMethod that = (RestMethod) obj;
    return Objects.equal(this.verb, that.verb)
        && Objects.equal(this.path, that.path);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("verb", verb)
        .add("path", path)
        .toString();
  }
}
