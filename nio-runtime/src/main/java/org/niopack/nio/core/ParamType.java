package org.niopack.nio.core;

public enum ParamType {
  QUERY(1),
  FORM(2),
  PATH(3);

  private final int value;

  private ParamType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
