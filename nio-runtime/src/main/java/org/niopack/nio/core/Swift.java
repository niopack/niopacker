package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Swift {
  private String module;

  public String getModule() {
    return module;
  }

  public void setModule(String module) {
    this.module = module;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(module);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Swift)) {
      return false;
    }
    Swift that = (Swift) obj;
    return Objects.equal(this.module, that.module);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("module", module)
        .toString();
  }
}
