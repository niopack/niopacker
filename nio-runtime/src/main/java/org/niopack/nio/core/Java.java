package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Java {
  private String packageName;

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(packageName);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Java)) {
      return false;
    }
    Java that = (Java) obj;
    return Objects.equal(this.packageName, that.packageName);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("packageName", packageName)
        .toString();
  }
}
