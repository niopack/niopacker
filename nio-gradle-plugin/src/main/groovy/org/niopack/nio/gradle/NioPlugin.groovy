package org.niopack.nio.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class NioPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.configurations {
            nio
        }

        project.repositories {
            mavenLocal()
            mavenCentral()
        }

        project.dependencies {
            nio 'org.niopacker:nio-compiler:0.7.1-SNAPSHOT'
        }

        project.task('niopacker', type:NiopackerTask)
    }
}
