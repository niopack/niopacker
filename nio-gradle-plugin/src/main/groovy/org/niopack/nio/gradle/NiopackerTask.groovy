package org.niopack.nio.gradle

import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class NiopackerTask extends JavaExec {
    @InputDirectory
    @Optional
    File root = project.file('src/main/nio')

    @InputDirectory
    @Optional
    File source = project.file('src/main/nio')

    @OutputDirectory
    @Optional
    File output = project.file("${project.buildDir}/gen/main/java")

    @Input
    @Optional
    List extraArgs

    NiopackerTask() {
        setMain('org.niopack.nio.compiler.tools.Tool')
        setClasspath(project.configurations.nio)
    }

    @TaskAction
    void exec() {
        args = niopackerArgs()
        super.exec()
    }

    private List niopackerArgs() {
        def args = ['JAVA', root, source, output]
        if (extraArgs) {
            args.addAll(extraArgs)
        }
        args
    }
}
