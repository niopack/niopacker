# Nio Gradle Plugin
[niopack-url]: http://www.niopack.org/
[issues]: https://bitbucket.org/niopack/nio-gradle-plugin/issues
[gradle-url]: http://gradle.org/

The nio-gradle-plugin provides an easy way to generate Java source code with [niopack][niopack-url] using [Gradle][gradle-url].

## Installation

Use the following snippet inside a Gradle build file:

`build.gradle`
```groovy
buildscript {
    repositories {
        mavenLocal()
    }
    dependencies {
        classpath 'org.niopack:nio-gradle-plugin:0.1-SNAPSHOT'
    }
}
repositories {
   mavenLocal()
}
apply plugin: 'org.niopack.nio.gradle'
```

## Configuration

The plugin adds a new task named `niopacker`. This task exposes 5 properties as part of its configuration

 - source:: where the source grammars are. Type: File. Default: `src/main/nio`.
 - output:: where generated sources go. Type: File. Default: `$buildDir/gen/main/provided`.
 - extraArgs:: extra arguments to pass to the nio tool. Type: List. Default: empty. Example: extraArgs=['-Werror']

## Usage
### Basic generation

Generating grammars can be done by calling the `niopacker` task:

```groovy
gradle niopacker
```

### Dependency on generated sources

In general, you will want your main project to depend on the generated sources for compilation. You can easily do
this by adding this configuration into your `build.gradle`:

`build.gradle`
```groovy
// make the Java compile task depend on the niopacker task
compileJava.dependsOn niopacker

// add the generated source files to the list of provided sources
sourceSets.main.provided.srcDirs += niopacker.output

// add nio to classpath
configurations {
   compile.extendsFrom niopacker
}
```
